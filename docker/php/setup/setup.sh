#!/bin/bash

echo -e "****************************************************"
echo -e "SETUP IMAGE PHP-FPM"
echo -e "****************************************************"

echo -e "ADD USER DEVELOPER"
useradd -ms /bin/bash -u 1000 developer

echo "GROUP SHARING for developer / WWW-DATA"
echo -e "putting www-data in developer group"
usermod -a -G www-data developer
echo -e "putting developer in www-data group"
usermod -a -G developer www-data

mkdir -p /app
chgrp -R www-data /app
chmod -R 775 /app

echo -e "****************************************************"
echo -e "END"
echo -e "****************************************************"
