<?php $options = json_decode($setting->details); ?>

<label class="switch">
	<?php $checked = (isset($setting->value) && $setting->value == 1); ?>
	@if (isset($options->on) && isset($options->off))
		<input type="checkbox" name="@include('admin.settings.types.value-key')" class="toggleswitch"
			   {{ $checked ? 'checked' : '' }}
			   data-on="{{ $options->on }}"
			   data-off="{{ $options->off }}">
	@else
		<input type="checkbox" name="@include('admin.settings.types.value-key')"
			   {{ $checked ? 'checked' : '' }} class="toggleswitch">
	@endif
	<span class="checkbox-slider round"></span>
</label>
