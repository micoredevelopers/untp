<div class="preloader-holder" id="page-preloader" style="transition: opacity 400ms">
    <div class="preloader">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<script type="text/javascript" defer>
    window.onload = function () {
        document.querySelector('#app').classList.remove('blurred-app');
        document.querySelector('#page-preloader').remove();
    }
</script>