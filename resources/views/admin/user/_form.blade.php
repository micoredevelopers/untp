<?php /** @var $edit \App\Models\User */ ?>
{{-- todo remove Form package --}}

<!-- Name Form Input -->
<div class="form-group">
    @include('admin.partials.crud.default', ['name' => 'name', 'title' => __('form.name')])
    {!! errorDisplay('name') !!}
</div>
<div class="form-group">
    @include('admin.partials.crud.default', ['name' => 'surname', 'title' =>  __('form.surname')])
    {!! errorDisplay('surname') !!}
</div>
<div class="form-group">
    @include('admin.partials.crud.default', ['name' => 'patronymic', 'title' =>  __('form.patronymic')])
    {!! errorDisplay('patronymic') !!}
</div>
<!-- Name Form Input -->
<div class="form-group">
    @include('admin.partials.crud.elements.phone')
    {!! errorDisplay('phone') !!}
</div>

<!-- email Form Input -->
<div class="form-group">
    @include('admin.partials.crud.elements.email')
    {!! errorDisplay('email') !!}
</div>

<!-- password Form Input -->
<div class="form-group @error('password') has-error @enderror">
    @include('admin.partials.crud.default',
    ['name' => 'password', 'title' =>__('auth.password'), 'value' => old('password', '')])
    {!! errorDisplay('password') !!}
</div>

<div class="row">
    <div class="col-3">
        @include('admin.partials.crud.elements.active')
    </div>
</div>