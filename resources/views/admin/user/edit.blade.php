<?php /** @var $edit \App\Models\User */ ?>

<main>
    <div class="ibox float-e-margins">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                {!! ViewHelper::tabHead('personal', 'Личные данные', true) !!}
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            {!! ViewHelper::openTabBody('personal', true) !!}
            <div class="ibox-content">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                @if($edit->last_login_ip ?? false)
                                    <div class="badge">Last IP:</div>
                                    <b>{{ $edit->last_login_ip }}</b>
                                @endif
                            </div>
                            <div class="col-md-2">
                                @if(isDateValid($edit->authenticated_at) ?? false)
                                    <div class="badge">Last login date</div>
                                    <b>{{ getDateFormatted($edit->authenticated_at) ?: 'Never' }}</b>
                                @endif
                            </div>
                            <div class="col-md-5">
                                @if($edit->user_agent ?? false)
                                    <div class="badge">User agent:</div>
                                    <b>{{ $edit->user_agent }}</b>
                                @endif
                            </div>
                            <div class="col-3">
                                @if (isSuperAdmin())
                                    <form action="{{ route('sign-super-admin') }}" method="post"
                                          data-form-confirm="true">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{ $edit->id }}">
                                        <button class="btn  btn-danger">
                                            <i class="fa fa-user-secret" aria-hidden="true"></i>Login as user
                                        </button>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <form method="POST" action="{{ route($routeKey . '.update', $edit->getKey()) }}" accept-charset="UTF-8">
                    @method('PUT')
                    @include('admin.user._form')
                    @include('admin.partials.submit_update_buttons')
                </form>
            </div>
            {!! ViewHelper::closeTab() !!}
        </div>
    </div>
</main>