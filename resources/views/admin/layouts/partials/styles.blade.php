
<link rel="stylesheet" href="{{ asset('_admin/css/flatpickr.css') }}">
<link rel="stylesheet" href="{{ asset('_admin/css/jquery.fancybox.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('_admin/libs/nestable2/jquery.nestable.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('_admin/css/bootstrap-select.css') }}">
@yield('css')
@stack('css')
<link rel="stylesheet" type="text/css"
	  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
<link rel="stylesheet" href="{{ asset('_admin/css/font-awesome.css') }}"/>