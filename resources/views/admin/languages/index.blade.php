<?php /** @var $list \App\Models\Language[]  */ ?>
<form action="{{route('languages.update-all')}}" method="post">
    <input type="hidden" name="_method" value="PATCH">
    @csrf
    <div class="table-responsive">
        <table class="table table-hover table-condensed table-striped">
            <tr class="active">
                <th>@lang('form.sorting')</th>
                <th>Ключ</th>
                <th>@lang('form.title')</th>
                <th>Включить в системе</th>
                <th>Отображать на сайте</th>
            </tr>
            @if(isset($list) AND $list->isNotEmpty())
                <tbody data-sortable-container="true" data-table="{{ $table ?? $list->isNotEmpty() ? $list->first()->getTable(): '' }}">
                @forelse($list as $item)
					<?php /** @var $item \App\Models\Admin\AdminMenu */ ?>
                    @php
                        $inputManager = inputNamesManager($item);
                    @endphp
                    <tr class="draggable" data-id="{{ $item->getKey() }}" data-sort="{{ $item->sort }}">
                        <input type="hidden" name="{{ $inputManager->getNameInputByKey('id') }}" value="{{ $item->getKey() }}">
                        <td>
                            @include('admin.partials.sort_handle')
                        </td>
                        <td>
                            <input type="text" class="form-control" readonly value="{{ $item->getLangKey() }}">
                        </td>
                        <td>
                            @includeIf('admin.partials.crud.default',
                            ['edit' => $item, 'name' => $inputManager->getNameInputByKey('name')])
                        </td>
                        <td>
                            @includeIf('admin.partials.crud.elements.active',
                            ['edit' => $item, 'name' => $inputManager->getNameInputByKey('active')])
                        </td>
                        <td>
                            @includeIf('admin.partials.crud.elements.active',
                            ['edit' => $item, 'name' => $inputManager->getNameInputByKey('display_front')])
                        </td>
                    </tr>
                @empty
                    <h3>Пусто</h3>
                @endforelse
                </tbody>
            @endif
        </table>
        <div class="text-right">
            <button class="btn btn-primary" type="submit">{{ __('form.save') }}</button>
        </div>
    </div>
</form>
