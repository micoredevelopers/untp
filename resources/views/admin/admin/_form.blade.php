<?php /** @var $edit \App\Models\User */ ?>
<!-- Name Form Input -->
<div class="form-group @if ($errors->has('fio')) has-error @endif">
    {!! Form::label('fio', 'ФИО') !!}
    {!! Form::text('fio', null, ['class' => 'form-control']) !!}
    {!! errorDisplay('fio') !!}
</div>
<!-- Name Form Input -->
<div class="form-group @if ($errors->has('phone')) has-error @endif">
    {!! Form::label('phone', __('form.phone')) !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    {!! errorDisplay('phone') !!}
</div>

<div class="form-group">
    @can('edit_roles')
        <div class="w-25 d-inline-block">
            <div class="form-group @if ($errors->has('roles')) has-error @endif">
                {!! Form::label('roles[]', 'Roles') !!}
                {!! Form::select('roles[]', $roles, isset($edit) ? $edit->roles->pluck('id')->toArray() : null,  ['class' => 'form-control selectpicker', 'multiple']) !!}
                {!! errorDisplay('roles') !!}
            </div>
        </div>
    @endcan
</div>
<div class="row">
	<div class="col-3">
		@include('admin.partials.crud.elements.active')
	</div>
</div>

<!-- Permissions -->

@can('edit_roles')
    @isset($edit)
        @include('admin.admin.includes._permissions', ['closed' => 'true', 'model' => $edit, 'user' => $edit ])
    @endisset
@endcan

