
<section class="about-us" id="about-us">
    <div class="container">
        <div class="about-us__statistics"><h2 class="about-us__statistics__title" data-aos="clip-top">Логістика &amp;
                Дистрибуція</h2>
            <div class="about-us__statistics__parasite">
                <div class="about-us__statistics__parasite__block"><img src="{{ asset('assets/img/audit.svg') }}" data-aos="clip-right"
                                                                        data-aos-delay="100">
                    <p data-aos="clip-top">Оцінка ВІДМІННО за результатами аудиту Ernst&amp;Young</p></div>
                <div class="about-us__statistics__parasite__block"><img src="{{ asset('assets/img/star.svg') }}" data-aos="clip-right"
                                                                        data-aos-delay="200">
                    <p data-aos="clip-top">Склади класу А, наявність лінії залізничних колій на території</p></div>
            </div>
            <div class="about-us__statistics__cards">
                <div class="about-us__statistics__card" data-aos="clip-top" data-aos-delay="300"><p class="year-value">
                        1997</p>
                    <p>рік заснування</p></div>
                <div class="about-us__statistics__card" data-aos="clip-top" data-aos-delay="300"><p class="regional-value">
                        6</p>
                    <p>областей</p></div>
                <div class="about-us__statistics__card" data-aos="clip-top" data-aos-delay="300"><p class="points-value">
                        11500 </p>
                    <p>точок</p></div>
                <div class="about-us__statistics__card" data-aos="clip-top" data-aos-delay="300"><p class="skuv-value">
                        3000 </p>
                    <p>SKU</p></div>
                <div class="about-us__statistics__card" data-aos="clip-top" data-aos-delay="300">
                    <div class="wrap"><p class="brands-value">100 </p>
                        <p>+</p></div>
                    <p>брендів</p></div>
                <div class="about-us__statistics__card" data-aos="clip-top" data-aos-delay="300"><p class="depart-value">
                        16 </p>
                    <p>відділів продажу</p></div>
            </div>
        </div>
        <div class="about-us__history"><img src="{{ asset('assets/img/store.jpg') }}" data-aos="clip-right" data-aos-delay="400">
            <p data-aos="clip-top" data-aos-delay="500">ПП "Юніон Трейд Плюс" - це великий національний холдинг, який займає одне з провідних місць на ринку дистрибуції FMCG України.
                Основна мета "UNTP" - управління і збільшення ефективності системи продажів, розвиток власного імпорту та торгових марок, побудова і модернізація логістичних процесів,  впровадження інновацій в сфері інформаційних технологій, створення та підтримка ефективної системи управління персоналом.
                ПП  "Юніон Трейд Плюс" визнане нашими партнерами як надійне, стабільне підприємство, що надає якісний  сервіс постачальникам і клієнтам в області продажів продуктів харчування та просуванні товарів масового попиту.
                Принцип роботи "UNTP" заснований на розумінні того, що люди є найбільш важливим ресурсом. З нами працюють понад 2000 успішних фахівців, які є головною складовою формули успіху Компанії. Ми прагнемо бути лідерами і тому продовжуємо пошук нових талановитих співробітників, з великим особистісним та професійним потенціалом, амбітних і відповідальних, готових йти до своєї мети.</p></div>
        <div class="about-us__group-compani"><h3 class="about-us__group-compani__title" data-aos="clip-top"
                                                 data-aos-delay="600">
                Крім дистрибуції UNTP до нашої групи компаній входять</h3>
            <div class="about-us__group-compani__cards">
                <a href="https://tochka.od.ua/" target="_blank" class="about-us__group-compani__card"><img src="{{ asset('assets/img/tochka.jpg') }}" data-aos="clip-right"
                                                                data-aos-delay="700"><img
                                class="logo tochka-logo" src="{{ asset('assets/img/tochka-logo.png') }}" data-aos="clip-right"
                                data-aos-delay="700">
                    <p data-aos="clip-top" data-aos-delay="700">Регіональна роздрібна мережа FMCG, магазини самообслуговування</p>
                </a>
                <a href="http://www.fortunacigars.com/ru/" target="_blank" class="about-us__group-compani__card"><img src="{{ asset('assets/img/fortuna.jpg') }}" data-aos="clip-right"
                                                                data-aos-delay="800">
                                                                <img
                                class="logo fortuna-logo" src="{{ asset('assets/img/fortuna-logo.png') }}" data-aos="clip-right"
                                data-aos-delay="800">
                    <p data-aos="clip-top" data-aos-delay="800">Імпортер, національний дистриб'ютор, національна мережа бутіків (сигари та тютюн)</p></a>
                <a href="https://bon-boisson.ua/" target="_blank" class="about-us__group-compani__card"><img src="{{ asset('assets/img/bon-buasson.jpg') }}" data-aos="clip-right"
                                                                data-aos-delay="900"><img
                                class="logo bon-buasson-logo" src="{{ asset('assets/img/bon-buasson-logo.svg') }}" data-aos="clip-right"
                                data-aos-delay="0">
                    <p data-aos="clip-top" data-aos-delay="0">Виробник, національний дистриб'ютор напоїв</p></a>
                <a href="https://idealmarket.net.ua/ua/" target="_blank" class="about-us__group-compani__card"><img src="{{ asset('assets/img/ideal.jpg') }}" data-aos="clip-right"
                                                                data-aos-delay="1000"><img
                                class="logo ideal-logo" src="{{ asset('assets/img/ideal-logo.png') }}" data-aos="clip-right"
                                data-aos-delay="0">
                    <p data-aos="clip-top" data-aos-delay="0">Регіональна оптово-роздрібна мережа cash&amp;carry</p></a>
            </div>
        </div>
    </div>
</section>