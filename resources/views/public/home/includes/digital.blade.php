
<section class="digital" id="digital">
    <div class="container"><h2 class="digital__title" data-aos="clip-top">Цифрові технології керування товарами</h2>
        <div class="digital__cards">
            <div class="digital__card">
                <div class="digital__card__wms" data-aos="clip-right" data-aos-delay="100"><img src="{{ asset('assets/img/wms.svg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="130">WMS Sedna</p>
                <p data-aos="clip-top" data-aos-delay="160">Розумний склад</p></div>
            <div class="digital__card">
                <div class="digital__card__ebp" data-aos="clip-right" data-aos-delay="200"><img src="{{ asset('assets/img/ebp.svg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="230">ERP Pike</p>
                <p data-aos="clip-top" data-aos-delay="260">Управління підприємством</p></div>
            <div class="digital__card">
                <div class="digital__card__mt" data-aos="clip-right" data-aos-delay="300"><img src="{{ asset('assets/img/mt.svg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="330">Pike MT</p>
                <p data-aos="clip-top" data-aos-delay="360">Мобільна торгівля</p></div>
            <div class="digital__card">
                <div class="digital__card__oracle" data-aos="clip-right" data-aos-delay="400"><img
                            src="{{ asset('assets/img/oracle.svg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="430">Oracle/ MS Power BI</p>
                <p data-aos="clip-top" data-aos-delay="460">Бізнес аналітика</p></div>
            <div class="digital__card">
                <div class="digital__card__emarket" data-aos="clip-right" data-aos-delay="500"><img
                            src="{{ asset('assets/img/emarket.svg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="530">EMarket</p>
                <p data-aos="clip-top" data-aos-delay="560">e-commerce</p></div>
            <div class="digital__card">
                <div class="digital__card__tms" data-aos="clip-right" data-aos-delay="600"><img src="{{ asset('assets/img/tms.svg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="630">TMS Ant</p>
                <p data-aos="clip-top" data-aos-delay="660">Транспортна логістика</p></div>
        </div>
        <div class="digital__description"><p data-aos="clip-top" data-aos-delay="0"><span></span> Власні розробки</p>
            <p data-aos="clip-top" data-aos-delay="100"><span></span> Власні розробки на сторонніх платформах</p>
            <p data-aos="clip-top" data-aos-delay="200"><span></span> Сторонні розробки</p></div>
    </div>
</section>