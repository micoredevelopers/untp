
<section class="logistics" id="logistics">
    <div class="container"><h2 class="logistics__title" data-aos="clip-top">Логістика</h2>
        <div class="logistics__indicators"><h3 class="logistics__indicators__title" data-aos="clip-top"
                                               data-aos-delay="100">
                                               Ключові показники роботи складів</h3>
            <div class="logistics__indicators__cards">
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="200"><p class="rc-value">7</p>
                    <p>РЦ</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="200"><p class="people-value">
                        155</p>
                    <p>чоловік</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="200"><p class="auto-value">
                        130</p>
                    <p>авто</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="300">
                    <div class="wrap"><p class="wms-value">100</p>
                        <p>% </p></div>
                    <p>WMS</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="300"><p class="sku-value">
                        3000</p>
                    <p>SKU</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="300"><p class="place-value">
                        11500</p>
                    <p>палетомість</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="400"><p class="forgive-value">
                        22100</p>
                    <p>м.кв.</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="400"><p class="ton-value">
                        2500</p>
                    <p>тонн в міс.</p></div>
                <div class="logistics__indicators__card" data-aos="clip-top" data-aos-delay="400"><p class="partners-value">
                        37</p>
                    <p>партнерів</p></div>
            </div>
        </div>
        <div class="logistics__chart logistics__chart-meters"><h3 class="logistics__chart__title" data-aos="clip-top"
                                                                  data-aos-delay="600">Квадратні метри</h3>
            <div class="wrap" data-aos="clip-down" data-aos-delay="600">
                <canvas class="canvas" width="953px" height="215px" id="bar-meters"></canvas>
            </div>
        </div>
        <div class="logistics__img"><img src="{{ asset('assets/img/logistics-img.jpg') }}" data-aos="clip-right" data-aos-delay="500">
        </div>
        {{-- <div class="logistics__chart logistics__chart-tons"><h3 class="logistics__chart__title" data-aos="clip-top"
                                                                data-aos-delay="700">Тонни на місяць</h3>
            <div class="wrap" data-aos="clip-down" data-aos-delay="700">
                <canvas class="canvas" width="953px" height="215px" id="bar-ton"></canvas>
            </div>
        </div> --}}
    </div>
</section>