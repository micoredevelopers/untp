
<section class="team" id="team">
    <div class="container">
        <div class="team__description"><h2 class="team__description__title" data-aos="clip-top">Наша команда</h2>
            {{-- <p class="team__description__text" data-aos="clip-top" data-aos-delay="100">Результат інвестицій у спорудження обладнання ІТ технології</p>--}}
            <a class="button-type-one team__description__button" 
                                                 href="{{ route('vacancy') }}"
                                                 data-aos="clip-top" data-aos-delay="200">Стати частиною команди</a></div>
        <div class="team__slider">
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" data-aos-delay="300" src="{{ asset('/assets/img/7.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Гринько Віталій Вікторович</p>
                <p data-aos="clip-top" data-aos-delay="300">Директор підприємства</p></div>
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" src="{{ asset('/assets/img/6.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Набєда Олексій Васильович</p>
                <p data-aos="clip-top" data-aos-delay="300">Директор з продажу</p></div>
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" src="{{ asset('/assets/img/1.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Хлистун Анжеліка Анатоліївна</p>
                <p data-aos="clip-top" data-aos-delay="300">Операційний директор</p></div>
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" src="{{ asset('/assets/img/5.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Пахольчак Альона Геннадіївна</p>
                <p data-aos="clip-top" data-aos-delay="300">Керівник відділу закупок</p></div>
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" src="{{ asset('/assets/img/4.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Воронцова Олена Олегівна</p>
                <p data-aos="clip-top" data-aos-delay="300">Координатор бізнес-напрямку Nestle</p></div>
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" src="{{ asset('/assets/img/3.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Савчук Андрій Павлович </p>
                <p data-aos="clip-top" data-aos-delay="300">Керівник напрямку</p></div>
            <div class="team__slider__card">
                <div class="team__slider__card__img-wrap">
                    <img data-aos="clip-right" src="{{ asset('/assets/img/2.jpg') }}">
                </div>
                <p data-aos="clip-top" data-aos-delay="300">Ніколайчук Василій Олександрович</p>
                <p data-aos="clip-top" data-aos-delay="300">Керівник напрямку</p></div>
        </div>
    </div>
</section>