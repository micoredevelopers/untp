<head>
{{--  не добавляй title, он генерится в функции ниже--}}
    {!! SEOMeta::generate() !!}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link href="{{ asset('static/css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('Logo.svg') }}" type="image/x-icon" />

</head>
