<?php /** @var $loop \App\Helpers\Dev\BladeLoopAutocompleteHelper */ ?>
@if($breadcrumbs ?? false)
    <div class="container">
        <div class="breadcrumbs">
            @foreach($breadcrumbs as $breadcrumb)
                @if (!$loop->last)
                    <a href="{{ $breadcrumb['url'] ?? '' }}" class="breadcrumb-item"
                       data-index="{{ $loop->iteration }}">{{ $breadcrumb['name'] ?? '' }}</a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                @else
                    <span class="breadcrumb-last">{{ $breadcrumb['name'] ?? '' }}</span>
                @endif
            @endforeach
        </div>
    </div>

    {{-- Долбаное сео, ненавижу --}}
    <script type="application/ld+json">
    [{
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [
            @foreach($breadcrumbs as $breadcrumb)
            @if (!$loop->last)
                {
                    "@type": "ListItem",
                    "position": {{ $loop->iteration }},
            "name": "{{ $breadcrumb['name'] ?? '' }}",
            "item": "{{ $breadcrumb['url'] ?? '' }}"
        },

            @else
                {
                    "@type": "ListItem",
                    "position": {{ $loop->iteration }},
                    "name": "{{ $breadcrumb['name'] ?? '' }}"
        }
            @endif
        @endforeach
        ]
      }]
      }]

    </script>
@endif