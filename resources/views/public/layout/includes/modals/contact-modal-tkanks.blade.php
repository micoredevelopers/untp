<div data-aos="clip-top" data-aos-delay="700" class="modal_bg contact-modal {{ session('successFeedback') ? 'active' : '' }}" id="thanks">
    <div class="container">
        <div class="contact-modal__wrapper">
            <div class="contact-modal__wrap"><h2 class="contact-modal__title mod-thanks">Дякую за заявку</h2>
                <button class="button-type-one close-modal-support">Окей</button>
                <button class="button-type-none close-modal-support">
                    <img src="{{ asset('assets/img/close.svg') }}">
                </button>
            </div>
        </div>
    </div>
</div>