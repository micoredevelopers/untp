<div>
    @push('js')
        <script type="text/javascript" defer>
            $(document).ready(function (e) {
                CKEDITOR.replace('{{ $elementId }}', {
                    filebrowserBrowseUrl: '/elfinder/ckeditor',
                    filebrowserImageBrowseUrl: '/elfinder/ckeditor',
                    language: '{{ $languageKey }}',
                    uiColor: '{{ $uiColor }}',
                    height: {{ $height }}
                });
            });
        </script>
    @endpush
</div>