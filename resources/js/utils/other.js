import IMask from 'imask';
import $ from 'jquery';

export const other = () => {
    window.addEventListener("load", () => {
        setTimeout(() => {
            const preloader = document.querySelector('[data-preloader]');
            if (!preloader){
                return;
            }
            document.body.classList.add('loaded');
            document.body.removeChild(preloader)
        }, 2150)
    })

    const elements = document.getElementsByClassName('imaskjs__input_tel');
    for (let i = 0; i < elements.length; i++) {
        new IMask(elements[i], {
            mask: '+{38}(000)000-00-00',
        });
    }
    const header = document.querySelector(".header")
    const section = document.querySelector("section")
    if (section.classList.contains("contacts-page")) {
        header.classList.toggle("contacts-mod")
    } else {
        header.classList.remove("contacts-mod")
    }

    // push form DATA ADD FILE 'form.php'
    function callForm(form) {
        const formAction = form.attr("action")
        const formMethod = form.attr("method")
        const msg = form.serialize();
        $.ajax({
            type: formMethod,
            url: formAction,
            data: msg
        });
    }
    
    
        $('#contact-form, #contact-modal-form, #cv-modal-form').on('submit', function (e) {
            e.preventDefault();
            callForm($(e.target));
        });

    const stateNames = document.querySelectorAll('[data-state]');
    stateNames.forEach(e=>{
        let stateModal = document.getElementById(`info-contact__${e.getAttribute("data-state")}`);
        let closeModal = document.getElementById(`close-modal-info__${e.getAttribute("data-state")}`);

        e.addEventListener("click", ()=>{
            stateModal.classList.toggle('active-info');
            
            closeModal.addEventListener("click", ()=>{
                stateModal.classList.remove('active-info');
            })
        })
    })
        

};
  