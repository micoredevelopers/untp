import {Chart} from "chart.js/dist/chart.min"
export const chart = () =>{

    let initialChart = (idName, dataOptions, titleLabels) => {
        const BarChart = document.getElementById(idName)
        new Chart(BarChart, {
            type: 'bar',
            data:{
                labels: ["Одеса 1997", "Миколаїв 2008", "Кропивницький 2010", "Херсон 2017", "Кривий Ріг 2018", "Запоріжжя  2019", "Дніпро 2021"],
                datasets: [
                    {
                        label: titleLabels,
                        backgroundColor: ["#2c287d"],
                        data: dataOptions
                    }
                ]
            } ,
            options: {
                plugins:{
                    legend: {display: false}},
                title: {
                    display: false,
                    text: 'Predicted'
                },
                animation: {
                    duration: 1500,
                    easing: 'ease',
                    tension: {
                        duration: 1000,
                        easing: 'easy',
                        from: 1,
                        to: 0,
                        loop: false,
                    }
                },
            }

        });
    }
    const dataForMeters = [10000, 1800, 1200, 1100, 1000, 1200, 3700];
    // const dataForTon = [1200, 175, 95, 80, 65, 120, 265];
    if (document.getElementById('bar-meters')){
        initialChart("bar-meters", dataForMeters, "Квадратні метри")
    }
    // if (document.getElementById('bar-ton')){
    //     initialChart("bar-ton", dataForTon, "Тонни на місяць")
    // }
}