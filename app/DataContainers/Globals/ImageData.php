<?php declare(strict_types=1);

namespace App\DataContainers\Globals;

use Illuminate\Support\Str;

class ImageData
{
    /**
     * @var string|null
     */
    private $pathToImage;

    public function __construct(?string $pathToImage)
    {
        $this->pathToImage = $pathToImage;
    }

    public function getOrigin(): ?string
    {
        return getPathToImage(imgPathOriginal($this->pathToImage));
    }

    public function getThumbnail()
    {
        if ($this->isThumbnail()) {
            return getPathToImage($this->pathToImage);
        }
        return getPathToImage(imgPathThumbnail($this->pathToImage));
    }

    private function isThumbnail(): bool
    {
        return Str::contains('_s.', $this->pathToImage);
    }
}