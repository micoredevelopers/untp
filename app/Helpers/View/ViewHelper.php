<?php

namespace App\Helpers\View;

class ViewHelper
{
	public static function openTabBody($id, $active = false)
	{
		return sprintf('<div class="tab-pane fade show %s" id="%s" role="tabpanel" aria-labelledby="%s-tab">',
			($active ? 'active' : ''),
			$id,
			$id,
		);
	}

	public static function closeTab()
	{
		return self::closeDiv();
	}

	public static function closeDiv()
	{
		return '</div>';
	}

	public static function openTabHead($id, $active)
	{
		return sprintf('<a class="nav-link text-dark %s"' .
			' id="%s-tab" data-toggle="tab" ' .
			'href="#%s" role="tab" ' .
			'aria-controls="%s" aria-selected="%s">',
			(!$active ?: 'active')
			, $id
			, $id
			, $id
			, json_encode($active)
		);
	}

	public static function closeTabHead()
	{
		return '</a>';
	}

	public static function tabHead($id, $name, $active = false)
	{
		return sprintf('%s %s %s', self::openTabHead($id, $active), $name, self::closeTabHead());
	}
}