<?php declare(strict_types=1);

namespace App\Enum;

final class TranslateFieldTypeEnum extends AbstractStrEnum
{
    public const TYPE_TEXT = 'text';
    public const TYPE_TEXTAREA = 'textarea';
    public const TYPE_EDITOR = 'editor';

    public static $enums = [
        'translate.field_type.text' => self::TYPE_TEXT,
        'translate.field_type.textarea' => self::TYPE_TEXTAREA,
        'translate.field_type.editor' => self::TYPE_EDITOR,
    ];


}