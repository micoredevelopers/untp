<?php

namespace App\Repositories\Admin;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortCriteria;
use App\Models\Language;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Collection;

/**
 * Class LanguageRepository.
 */
class LanguageRepository extends AbstractRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Language::class;
    }

    public function addPublicCriteriaToQuery(): self
    {
        $this->pushCriteria($this->app->make(SortCriteria::class));
        $this->pushCriteria($this->app->make(ActiveCriteria::class));
        return $this;
    }

    public function getForCreateEntity(): Collection
    {
        return Language::whereActive(1)->get();
    }

    public function findOneByCode($code)
    {
        $this->model = $this->model->where('active', 1)->where('key', $code);

        return $this->first();
    }

    public function getDefaultLanguage()
    {
        $this->model = $this->model->where('active', 1)->where('default', 1);

        return $this->first();
    }

    public function getListPublic(): Collection
    {
        $this->addPublicCriteriaToQuery();
        $this->where('display_front', true);
        return $this->all();
    }
}
