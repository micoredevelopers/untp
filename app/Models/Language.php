<?php

namespace App\Models;


/**
 * App\Models\Language
 *
 * @property int $id
 * @property int $active
 * @property int $display_front
 * @property int $default
 * @property string|null $name
 * @property string $key
 * @property string|null $icon
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class Language extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'active' => 'bool'
    ];

    protected $fillable = [
        'active',
        'name',
        'icon',
    ];

    public function scopeDefault($query)
    {
        return $query->where('default', '=', 1);
    }

    public function setName(string $name): self
    {
        $this->attributes['name'] = $name;
        return $this;
    }

    public function setKey(string $value): self
    {
        $this->attributes['key'] = $value;
        return $this;
    }

    public function setActive(bool $active): self
    {
        $this->attributes['active'] = $active;
        return $this;
    }

    public function setDisplayFront(bool $value): self
    {
        $this->attributes['display_front'] = $value;
        return $this;
    }

    public function getLangKey(): string
    {
        return (string)$this->getAttribute('key');
    }

    public function setSort(int $sort)
    {
        $this->setAttribute('sort', $sort);
        return $this;
    }

    public function isDefault()
    {
        return (bool)$this->getAttribute('default');
    }
}
