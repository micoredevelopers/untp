<?php

namespace App\View\Components\Admin\View\Plugin;

use Illuminate\View\Component;

class Ckeditor extends Component
{
    public $elementId;
    public $languageKey;
    public $uiColor;
    public $height;

    public function __construct(
        $elementId,
        $languageKey = null,
        $uiColor = '#9AB8F3',
        $height = 300
    )
    {
        $this->elementId = $elementId;
        $this->languageKey = $languageKey ?? app()->getLocale();
        $this->uiColor = $uiColor;
        $this->height = $height;
    }

    public function render()
    {
        return view('components.admin.view.plugin.ckeditor');
    }
}
