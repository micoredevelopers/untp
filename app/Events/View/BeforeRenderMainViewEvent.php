<?php

namespace App\Events\View;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BeforeRenderMainViewEvent
{
    use Dispatchable, SerializesModels;

    public function __construct()
    {
        //
    }

}
