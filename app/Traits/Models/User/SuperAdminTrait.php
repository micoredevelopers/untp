<?php

namespace App\Traits\Models\User;


use App\Models\Role;
use App\Models\User;

trait SuperAdminTrait
{

    public function isSuperAdmin(): bool
    {
        return (int)$this->id === (int)config('permission.super_admin_id');
    }

    public function isAdmin(): bool
    {
        static $isAdmin = null;
        if ($isAdmin === null) {
            /** @var  $user User */
            $isAdmin = ($this->hasAnyRole(Role::getAllRoles()));
        }

        return $isAdmin;
    }
}