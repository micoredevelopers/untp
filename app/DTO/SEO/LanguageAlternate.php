<?php declare(strict_types=1);

namespace App\DTO\SEO;

class LanguageAlternate
{
    private $rel = 'alternate';

    private $hreflang = 'x-default';

    private $href = '';

    /**
     * @return string
     */
    public function getRel(): string
    {
        return $this->rel;
    }

    /**
     * @param string $rel
     * @return LanguageAlternate
     */
    public function setRel(string $rel): LanguageAlternate
    {
        $this->rel = $rel;
        return $this;
    }

    /**
     * @return string
     */
    public function getHreflang(): string
    {
        return $this->hreflang;
    }

    /**
     * @param string $hreflang
     * @return LanguageAlternate
     */
    public function setHreflang(string $hreflang): LanguageAlternate
    {
        $this->hreflang = $hreflang;
        return $this;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @param string $href
     * @return LanguageAlternate
     */
    public function setHref(string $href): LanguageAlternate
    {
        $this->href = $href;
        return $this;
    }

    public static function create(string $url): self
    {
        return (new self())->setHref($url);
    }

}