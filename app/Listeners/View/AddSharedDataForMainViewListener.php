<?php

namespace App\Listeners\View;

use App\Helpers\LanguageHelper;
use App\Repositories\Admin\LanguageRepository;
use App\Repositories\MenuRepository;

class AddSharedDataForMainViewListener
{
    public function handle($event)
    {
        $languages = app(LanguageRepository::class)->getListPublic();
        view()->share(
            compact(
                'languages'
            )
        );
    }
}
