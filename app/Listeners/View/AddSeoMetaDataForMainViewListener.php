<?php

namespace App\Listeners\View;

use App\DTO\SEO\LanguageAlternate;
use App\Helpers\Miscellaneous\Breadcrumbs;
use App\Models\Meta;
use Artesaos\SEOTools\Facades\SEOMeta;

class AddSeoMetaDataForMainViewListener
{
    public function __construct()
    {
    }

    public function handle($event)
    {
        $this->meta();
        $this->canonical();
        $this->langAlternates();
        $this->breadcrumbs();

    }

    private function meta()
    {
        if ($metadata = Meta::getMetaData()) {
            if ($metadata->title) {
                SEOMeta::setTitle($metadata->title, true);
            }
            if ($metadata->description) {
                SEOMeta::setDescription($metadata->description, true);
            }
            if ($metadata->keywords) {
                SEOMeta::setKeywords($metadata->keywords, true);
            }
            return;
        }

        if ((!SEOMeta::getTitle() || !SEOMeta::getDescription() || !SEOMeta::getKeywords()) && ($meta_default = Meta::getDefaultMeta())) {
            if (!SEOMeta::getTitle()) {
                SEOMeta::setTitle($meta_default->title);
            }
            if (!SEOMeta::getDescription()) {
                SEOMeta::setDescription($meta_default->description);
            }
            if (!SEOMeta::getKeywords()) {
                SEOMeta::setKeywords($meta_default->keywords);
            }
        }
    }

    private function canonical()
    {
        if (request()->has('page')) {
            SEOMeta::setCanonical(url()->current());
        }
    }

    private function langAlternates()
    {
        $currentUrl = getNonLocaledUrl();
        foreach (\LaravelLocalization::getSupportedLocales() as $langKey => $item) {
            $urlLocale = langUrl($currentUrl, $langKey);
            $langs[$langKey] = LanguageAlternate::create($urlLocale)
                ->setHreflang($item['regional']);
        }
        $langs[] = LanguageAlternate::create($currentUrl)->setHreflang('x-default');
        view()->share([
            'langAlternatives' => $langs ?? []
        ]);
    }

    private function breadcrumbs(){
        view()->share([
            'breadcrumbs' => Breadcrumbs::getBreadCrumbs()
        ]);
    }
}

