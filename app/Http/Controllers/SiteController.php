<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\View\BeforeRenderMainViewEvent;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class SiteController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->addBreadCrumb(getSetting('global.home-bread-name'), route('home'));
    }

    public function main($data = [])
    {
        event(new BeforeRenderMainViewEvent());
        return view('public.layout.app', $data);
    }

    protected function setNextPrevLinkForPagination(LengthAwarePaginator $paginator): void
    {
        $nextUrl = $paginator->nextPageUrl();
        $prevUrl = $paginator->previousPageUrl();

        if ($paginator->currentPage() === 2) {
            $this->setPrev(url()->current());
        } else if ($prevUrl !== null) {
            $this->setPrev($prevUrl);
        }
        if ($paginator->hasMorePages()) {
            $this->setNext($nextUrl);
        }
    }
}















