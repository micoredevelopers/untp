<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Staff;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Language;
use App\Repositories\Admin\LanguageRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class LanguagesController extends AdminController
{
    use Authorizable;

    private $name = 'Languages';

    protected $permissionKey = 'languages';

    protected $routeKey = 'admin.languages';

    protected $key = 'languages';
    /**
     * @var LanguageRepository
     */
    private $repository;

    public function __construct(LanguageRepository $repository)
    {
        parent::__construct();
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));
        $this->shareViewModuleData();
        $this->repository = $repository;
    }

    public function index()
    {
        $list = $this->repository->all();
        $this->setTitle($this->name);

        $with = compact(array_keys(get_defined_vars()));
        $data['content'] = view('admin.languages.index', $with);

        return $this->main($data);
    }

    public function updateAll(Request $request)
    {
        foreach ($request->get('languages') as $item) {
            if (!$item['id'] ?? null){
                continue;
            }
            /** @var  $language Language */
            $language = $this->repository->find($item['id']);
            if (!$language) {
                continue;
            }
            $active = (bool)($item['active'] ?? 0);
            $displayFront = (bool)($item['display_front'] ?? 0);
            if ($language->isDefault()){
                $active = $displayFront = true;
            }
            $language->setActive($active)->setName($item['name'])->setDisplayFront($displayFront)->save();
        }
        $this->setSuccessUpdate();
        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }
}