<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Admin;

use App\DataContainers\Admin\Admin\SearchDataContainer;
use App\Http\Controllers\Admin\AdminController as AbstractAdminController;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Http\Requests\Admin\UserProfileRequest;
use App\Models\Admin\Admin;
use App\Models\Permission;
use App\Models\Role;
use App\Repositories\AdminRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends AbstractAdminController
{
    use Authorizable;

    protected $routeKey = 'admin.admins';

    protected $permissionKey = 'admins';

    protected $key = 'admins';

    protected $name;
    /**
     * @var AdminRepository
     */
    private $repository;
    /**
     * @var SearchDataContainer
     */
    private $searchDataContainer;

    public function __construct(AdminRepository $repository, SearchDataContainer $searchDataContainer)
    {
        parent::__construct();
        $this->name = __('modules.admins.title');
        $this->addBreadCrumb(__('modules.admins.title'), $this->resourceRoute('index'));
        $this->shareViewModuleData();
        $this->repository = $repository;

        $searchDataContainer->setIsSuperAdmin(isSuperAdmin());
        $this->searchDataContainer = $searchDataContainer;
    }

    public function index(Request $request)
    {
        $title = $this->name;
        $this->setTitle($title);
        if ($search = (string)$request->get('search')) {
            $this->searchDataContainer->setSearch($search);
        }

        $result = $this->repository->getListAdmin($this->searchDataContainer);
        $data['content'] = view('admin.admin.index', compact('result', 'search'));

        return $this->main($data);
    }

    public function create(Admin $admin)
    {
        $title = __('form.create');
        $this->setTitle($title)->addBreadCrumb($title);
        $roles = Role::pluck('name', 'id');
        $data['content'] = view('admin.admin.create', compact('roles'));

        return $this->main($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'nullable|email|unique:admins',
            'password' => 'required|min:6',
        ]);
        $this->setSuccessStore();
        $request->merge(['password' => Hash::make($request->get('password'))]);
        // Create the admin
        $input = $request->except('roles', 'permissions');
        $input['active'] = (int)$request->get('active');

        if (($admin = new Admin)->fillExisting($input)->save()) {
            $this->syncPermissions($request, $admin);
        }
        if ($request->has('createOpen')) {
            return redirect($this->resourceRoute('edit', $admin->id))->with($this->getResponseMessage());
        }

        return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
    }

    public function edit($id)
    {
        $edit = $this->repository->applyFilter($this->searchDataContainer)->find($id);
        $title = $this->titleEdit($edit);
        $this->addBreadCrumb($title)->setTitle($title);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::getList();
        $data['content'] = view('admin.admin.edit', compact('edit', 'roles', 'permissions'));

        return $this->main($data);
    }

    public function update(UpdateUserRequest $request, Admin $admin)
    {
        $this->setSuccessUpdate();
        // Get the admin
        $admin = $this->repository->applyFilter($this->searchDataContainer)->find($admin->id);
        // Update admin
        $input = $request->except('roles', 'permissions', 'password');
        $admin->fillExisting($input);
        if ($request->get('password')) {
            $admin->password = bcrypt($request->get('password'));
        }
        $this->syncPermissions($request, $admin);
        if ($admin->save()) {
            $this->setSuccessUpdate();
        }

        if ($request->has('saveClose')) {
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if (Auth::guard('admin')->user()->id == $id) {
            $this->setFailMessage('Deletion of currently logged in admin is not allowed :(');

            return redirect()->back()->with($this->getResponseMessage());
        }
        if ($this->repository->applyFilter($this->searchDataContainer)->find($id)->delete()) {
            $this->setSuccessMessage('Admin has been deleted');
        } else {
            $this->setFailMessage('Admin not deleted');
        }

        return redirect()->back();
    }

    private function syncPermissions(Request $request, $admin)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);
        // Get the roles
        $roles = Role::find($roles);
        // check for current role changes
        if (!$admin->hasAllRoles($roles)) {
            // reset all direct permissions for admin
            $admin->permissions()->sync([]);
        } else {
            // handle permissions
            $admin->syncPermissions($permissions);
        }
        $admin->syncRoles($roles);

        return $admin;
    }

    public function profile()
    {
        $locales = \LaravelLocalization::getSupportedLocales();
        $this->dropLastBreadCrumb();
        $title = __('modules.admins.profile.title');
        $this->addBreadCrumb($title)->setTitle($title);
        $data['content'] = view('admin.admin.profile', [
            'admin' => Auth::guard('admin')->user(),
            'locales' => $locales,
        ]);

        return $this->main($data);
    }

    /**
     * @param UserProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profileUpdate(UserProfileRequest $request)
    {
        /** @var $admin \App\Models\Admin\Admin */
        $admin = \Auth::guard('admin')->user();
        if ($request->isPasswordsWasSend()) {
            $passwordsMatches = Hash::check($request->get('password'), $admin->password);
            if ($passwordsMatches) {
                $admin->password = Hash::make($request->get('password_new'));
            } else {
                $this->setMessage(__('admin.invalid-current-password'));

                return back()->with($this->getResponseMessage())->withInput($request->input());
            }
        }
        $this->setSuccessUpdate();
        $data = $request->only([
            'locale',
            'name',
        ]);
        $admin->fillExisting($data)->save();

        return redirect()->back()->with($this->getResponseMessage());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function signSuperAdmin(Request $request)
    {
        $this->authorize('superadmin');
        if ($adminId = (int)$request->get('admin_id')) {
            if ($admin = Admin::find($adminId)) {
                session()->flash('superadmin-login');
                \Auth::guard()->login($admin);
            }
        }

        return redirect()->back();
    }
}
