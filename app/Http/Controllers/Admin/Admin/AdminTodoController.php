<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Admin;

use App\Http\Controllers\Admin\AdminController as AbstractAdminController;
use App\Traits\Authorizable;

class AdminTodoController extends AbstractAdminController
{
    use Authorizable;

    protected $routeKey = 'admin.todo';

    protected $permissionKey = 'todo';

    protected $key = 'todo';

    public function __construct()
    {
        parent::__construct();
        $this->addBreadCrumb(__('modules.users.title'), $this->resourceRoute('index'));
        $this->shareViewModuleData();

        $this->setTitle('TODO');
    }

    public function index()
    {
        $data['content'] = view('admin.admin.todo.todo');

        return $this->main($data);
    }

}
