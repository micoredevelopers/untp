<?php

namespace App\Http\Middleware;

use App\Models\Redirect;
use App\Repositories\RedirectRepository;
use Closure;

class RedirectsMiddleware
{
    private $redirectRepository;

    public function __construct(RedirectRepository $redirectRepository)
    {
        $this->redirectRepository = $redirectRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = parse_url(getNonLocaledUrl(), PHP_URL_PATH);
        if ($redirect = $this->redirectRepository->findByUrl((string)$url)) {
			return redirect($redirect->to, $redirect->code);
        }
        return $next($request);
    }
}
