<?php

use App\Http\Controllers\Admin\TranslateController;

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localizationRedirect'],
], static function () {
    Route::middleware(['auth-admin:admin', 'bindings'])
        ->prefix('admin')
        ->group(function () {

            Route::get('/translate/seed', [TranslateController::class, 'seed'])->name('translate.seed');
            Route::resource('translate',TranslateController::class);

        });
});