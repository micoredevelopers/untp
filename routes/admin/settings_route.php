<?php

use App\Http\Controllers\Admin\SettingController;

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localizationRedirect'],
], static function () {
    Route::middleware(['auth-admin:admin', 'bindings'])
        ->prefix('admin')
        ->group(function () {

            Route::group(['as' => 'settings.', 'prefix' => 'settings',], static function () {
                Route::get('/seed', [SettingController::class, 'seed'])->name('seed');
                Route::get('{id}/delete_value', [
                    'uses' => [SettingController::class, 'delete_value'],
                    'as' => 'delete_value',
                ]);
            });

            Route::resource('settings', SettingController::class);
        });
});