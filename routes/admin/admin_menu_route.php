<?php


use App\Http\Controllers\Admin\AdminMenuController;

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localizationRedirect'],
], static function () {
    Route::middleware(['auth-admin:admin', 'bindings'])
        ->prefix('admin')
        ->group(function () {
            Route::resource(
                'admin-menus', AdminMenuController::class,
                [
                    'as' => 'admin',
                    'except' => ['show']
                ]
            );

            Route::post('/seed', [AdminMenuController::class, 'seed'])->name('admin.admin-menus.seed');

            Route::post('/admin-menus/nesting', [AdminMenuController::class, 'nesting'])->name('admin.admin-menus.nesting');
            Route::patch('/admin-menus/save/all', [AdminMenuController::class, 'updateAll'])->name('admin-menus.updateAll');

        });
});